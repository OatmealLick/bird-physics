extends KinematicBody2D

const MAX_FALLING_VELOCITY: float = 180.0
const HORIZONTAL_VELOCITY: float = 4.0
const JUMP_FORCE: float = 5.0
const GRAVITY: float = 10.0

var playing: bool = false  # just to start the game with first click
var velocity: Vector2 = Vector2(HORIZONTAL_VELOCITY, 0.0)


func _physics_process(delta):
	var pressed_jump = Input.is_action_just_pressed("ui_accept")
	
	if not playing and pressed_jump:
		playing = true
	elif not playing:
		return
	
	if pressed_jump:
		velocity.y = -JUMP_FORCE
	
	if velocity.y < MAX_FALLING_VELOCITY:
		velocity.y += delta * GRAVITY
	else:
		velocity.y = MAX_FALLING_VELOCITY
	var collision: KinematicCollision2D = move_and_collide(velocity)
	if collision:
		velocity.x = -velocity.x
