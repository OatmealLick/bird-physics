extends KinematicBody2D

signal velocity_changed(velocity)

const MAX_FALLING_VELOCITY: float = 5.0
const HORIZONTAL_VELOCITY: float = 0.5
const GRAVITY: float = 2.0
const ROTATION_SPEED: float = 3.0

var velocity: Vector2 = Vector2(HORIZONTAL_VELOCITY, 0.0)
var last_velocity

func _physics_process(delta):
	
	velocity = _set_direction_based_on_input(delta)
	
	# making sure bird does not accelerate to the speed of light (is it necessary?)
	if velocity.y < MAX_FALLING_VELOCITY:
		var normalized_scale = velocity.normalized().dot(Vector2.DOWN)
		velocity *= (1 + normalized_scale * GRAVITY * delta)
		var radians = velocity.angle_to(Vector2.DOWN)
		var degrees = rad2deg(radians)
		$Sprite.set_rotation_degrees(-degrees)
	else:
		velocity.y = MAX_FALLING_VELOCITY
	var collision: KinematicCollision2D = move_and_collide(velocity)
	if collision:
		velocity.x = -velocity.x
	
	emit_signal("velocity_changed", velocity)
	last_velocity = velocity

func _set_direction_based_on_input(delta):
	if Input.is_action_pressed("right"):
		return velocity.rotated(ROTATION_SPEED * delta)
	elif Input.is_action_pressed("left"):
		return velocity.rotated(ROTATION_SPEED * delta * -1)
	else:
		return velocity
