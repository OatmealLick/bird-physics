extends CanvasLayer

onready var _velocity_label = $MarginContainer/VBoxContainer/Label


func _on_bird_velocity_changed(velocity):
	_velocity_label.text = "Velocity: %s" % velocity
