extends RigidBody2D

const MAX_FALLING_VELOCITY: float = 180.0

var force: float = 300.0
var playing: bool = false

func _integrate_forces(state):
	if state.linear_velocity.y > MAX_FALLING_VELOCITY:
		state.linear_velocity.y = MAX_FALLING_VELOCITY
		
		
	if Input.is_action_just_pressed("ui_accept"):
		if not playing:
			playing = true
			gravity_scale = 3.0
		apply_central_impulse(Vector2.UP * force)
		
